//
//  TransportViewController.swift
//  AdvancedDrumMachineApp
//
//  Created by Morten Robinson on 20/12/2018.
//  Copyright © 2018 Morten Robinson. All rights reserved.
//

import UIKit

class TransportViewController: UIViewController {
    
    @IBOutlet weak var viewSelect: UISegmentedControl!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var BPMSlider: UISlider!
    @IBOutlet weak var metronomeButton: UIButton!
    @IBOutlet weak var changeKit: UIStepper!
    
    var recording = false

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        NotificationCenter.default.addObserver(self, selector: #selector(patternChanged(n: )), name: NSNotification.Name.init(rawValue: "patternChanged"), object: nil)
        
        //Add listeners for the pads when recording
        for i in 0...9 {
            NotificationCenter.default.addObserver(self, selector: #selector(recordPattern(n: )), name: NSNotification.Name.init(rawValue: "pad\(i)"), object: nil)
        }
        
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.5529922447, green: 0.7101133869, blue: 0.8919535216, alpha: 1);
        viewChange(viewSelect)
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    //This function gets called everytime pad notification is observed
    @objc func recordPattern(n: NSNotification){
        //We can ignore the notification if we're not recording
        if recording {
            
            //Gets the nearest quantized position from the sequencer. The quantization is one quarte of a beat. The format is beats and since one beat is 4 steps or 4 1/16th notes this needs to be multiplied by 4 in order to convert to steps
            var position = Int((padViewController.pads.sequencer.nearestQuantizedPosition(quantizationInBeats: 1/4).beats)*4)
            
            //sometimes when close the end of the sequence length the position can quantize to 16 which is out of range.
            if position > 15 {
                position = 0
            }
            
            //Step position in the pattern for the corresponding pad will be set to true.
            switch n.name.rawValue {
            case "pad0":
                patternViewController.pattern.pattern[0][position] = true
                patternViewController.updateView()
            case "pad1":
                patternViewController.pattern.pattern[1][position] = true
                patternViewController.updateView()
            case "pad2":
                patternViewController.pattern.pattern[2][position] = true
                patternViewController.updateView()
            case "pad3":
                patternViewController.pattern.pattern[3][position] = true
                patternViewController.updateView()
            case "pad4":
                patternViewController.pattern.pattern[4][position] = true
                patternViewController.updateView()
            case "pad5":
                patternViewController.pattern.pattern[5][position] = true
                patternViewController.updateView()
            case "pad6":
                patternViewController.pattern.pattern[6][position] = true
                patternViewController.updateView()
            case "pad7":
                patternViewController.pattern.pattern[7][position] = true
                patternViewController.updateView()
            case "pad8":
                patternViewController.pattern.pattern[8][position] = true
                patternViewController.updateView()
            case "pad9":
                patternViewController.pattern.pattern[9][position] = true
                patternViewController.updateView()
            default:
                break
            }
        }
    }
    

    
    
    
    @IBAction func viewChange(_ sender: UISegmentedControl) {
        if viewSelect.selectedSegmentIndex == 0 {
            remove(asChildViewController: patternViewController)
            add(asChildViewController: padViewController)
        }
        else {
            remove(asChildViewController: padViewController)
            add(asChildViewController: patternViewController)
        }
    }
    
    
    
    

    @IBAction func BPMChange(_ sender: UISlider) {
        padViewController.pads.BPM = Double(sender.value)

    }
    
    
    
    
    private lazy var padViewController: PadViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "padViewController") as! PadViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    
    
    private lazy var patternViewController: PatternViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "patternViewController") as! PatternViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
    
    
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChild(viewController)
        
        // Add Child View as Subview
        view.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = view.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParent: self)
    }
    
    
    
    
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParent: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParent()
    }
    
    
    
    
    
    //make sure sequecer gets updated when there a pattern change notification is observed.
    @objc func patternChanged(n: NSNotification){
        padViewController.pads.updateSequence(pattern: patternViewController.pattern)
    }
    
    
    
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        padViewController.pads.sequencer.play()
        padViewController.pads.metronome.play()
        playButton.setTitleColor(#colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1), for: UIControl.State.normal)
    }
    
    
    
    
    
    @IBAction func stopButtonPressed(_ sender: Any) {
        padViewController.pads.sequencer.stop()
        padViewController.pads.sequencer.rewind()
        padViewController.pads.metronome.stop()
        playButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControl.State.normal)
        recordButton.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: UIControl.State.normal)
        recording = false
    }
    
    
    
    
    
    
    @IBAction func recordButtonPressed(_ sender: Any) {
        recordButton.setTitleColor(#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1), for: UIControl.State.normal)
        playButton.setTitleColor(#colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1), for: UIControl.State.normal)
        recording = true
        padViewController.pads.metronome.play()
        padViewController.pads.sequencer.play()
    }
    
    
    
    
    
    @IBAction func metronomeButtonPressed(_ sender: UIButton) {
        if metronomeButton.title(for: UIControl.State.normal) == "🔕" {
            padViewController.pads.metronome.metronomeOnOff(state: true)
            metronomeButton.setTitle("🔔", for: UIControl.State.normal)
        } else {
            padViewController.pads.metronome.metronomeOnOff(state: false)
            metronomeButton.setTitle("🔕", for: UIControl.State.normal)
        }
    }
    
    
    
    
    @IBAction func changeKit(_ sender: UIStepper) {
        padViewController.pads.chooseKit = Int(sender.value)
        padViewController.pads.changeKit()
    }
    
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        patternViewController.pattern.clearPattern()
        patternViewController.updateView()
    }
}
