//
//  Pads.swift
//  AdvancedDrumMachineApp
//
//  Created by Morten Robinson on 01/12/2018.
//  Copyright © 2018 Morten Robinson. All rights reserved.
//

import Foundation
import AudioKit

struct Pads {
    
    let folder = ["BassDrums", "SnareDrums", "ClosedHiHats", "Conga", "SFX", "Toms", "Percussive", "Claps", "OpenHiHats", "Crash"]
    let file = ["BDXX","SDSDXXXX","CHHXX","CONGAXX","SFXX","TOMXX","PERCXX","CPXX","OHHXX","CRSHXX"]
    
    var drumPadSamplers = [AKMIDISampler]()
    var samples = [AKAudioFile]()
    var chooseKit = 0
    var mixer = AKMixer()
    var BPM = 128.0 {
        didSet {
            sequencer.setTempo(BPM)
            metronome.sequencer.setTempo(BPM)
        }
    }
    
    let sequenceLength = AKDuration(beats: 4.0)
    var sequencer = AKSequencer()
    let metronome = Metronome()

    
    init(numberOfPads: Int) {
        
        AudioKit.output = mixer
        //Start AudioKit
        do {
            try AudioKit.start()
        } catch {
            AKLog("AudioKit did not start!")
        }
        
        for pads in 0...numberOfPads {
            drumPadSamplers.append(AKMIDISampler())
            do {
                let sample = try AKAudioFile(readFileName: "/Samples/\(folder[pads])/\(chooseKit)\(file[pads]).wav")
                try drumPadSamplers[pads].loadAudioFile(sample)
            } catch{
                do {
                    let sample = try AKAudioFile(readFileName: "/Samples/\(folder[pads])/\(chooseKit)\(file[pads]).aif")
                    try drumPadSamplers[pads].loadAudioFile(sample)
                } catch {
                    AKLog("\(pads)\(file) did not load")
                }
            }
        }
        initSequencer()
        drumPadSamplers >>> mixer
    }
    
    //Changes between 10 different kits with different sounds
    func changeKit(){
        for pads in 0...9{
            do {
                let sample = try AKAudioFile(readFileName: "/Samples/\(folder[pads])/\(chooseKit)\(file[pads]).wav")
                try drumPadSamplers[pads].loadAudioFile(sample)
            } catch{
                do {
                    let sample = try AKAudioFile(readFileName: "/Samples/\(folder[pads])/\(chooseKit)\(file[pads]).aif")
                    try drumPadSamplers[pads].loadAudioFile(sample)
                } catch {
                    AKLog("\(pads)\(file) did not load")
                }
            }
        }
    }
    
    private func initSequencer(){
        //Add tracks to the sequencer for every drumpad
        for i in 0...9 {
            drumPadSamplers[i].enableMIDI()
            drumPadSamplers[i].volume = 64
            _ = sequencer.newTrack()
            sequencer.tracks[i].setMIDIOutput(drumPadSamplers[i].midiIn)
        }
        sequencer.setLength(sequenceLength)
        sequencer.enableLooping(sequenceLength)
    }
    
    func updateSequence(pattern: Pattern){
            sequencer.clearRange(start: AKDuration(beats: 0), duration: sequenceLength)
            for pad in 0..<pattern.pattern.count {
                for step in 0..<pattern.pattern[pad].count {
                    if pattern.pattern[pad][step] == true {
                        //This looks weird, but has something to do with the file names of these drum samples. As default AudioKit will assign the root key of the sample (the key where the sample is played at normal speed) to midi note number 60. But AudioKit can also assign it to a specific note based on it's filename. For some reason sample 1 and 9 have been assigned with midi note number 0 as the root key and it has something to do with their filenames.
                        if pad != 1 && pad != 9 {
                            sequencer.tracks[pad].add(noteNumber: 60, velocity: 100, position: AKDuration(beats: Double(step) / 4.0), duration: AKDuration(beats: 1))
                        } else {
                            sequencer.tracks[pad].add(noteNumber: 0, velocity: 100, position: AKDuration(beats: Double(step) / 4.0), duration: AKDuration(beats: 1))
                        }
                    }
                }
            }
    }
    
    func oneShot(pad: Int){
        do {
            if(pad != 9 && pad != 1) {
                try drumPadSamplers[pad].play(noteNumber: 60, velocity: 100, channel: 0)
            } else {
                try drumPadSamplers[pad].play(noteNumber: 0, velocity: 100, channel: 0)
            }
        } catch {
            AKLog("Could Not Play")
        }
    }
}
