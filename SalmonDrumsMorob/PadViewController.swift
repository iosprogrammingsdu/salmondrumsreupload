//
//  ViewController.swift
//  AdvancedDrumMachineApp
//
//  Created by Morten Robinson on 30/11/2018.
//  Copyright © 2018 Morten Robinson. All rights reserved.
//

import UIKit

class PadViewController: UIViewController {
    
    var pads: Pads = Pads(numberOfPads: 10 - 1)

    var numberOfPads: Int {
        return padButtons.count + 1
    }
    
    @IBOutlet var padButtons: [UIButton]!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    
    
    @IBAction func padTapped(_ sender: UIButton) {
        if let iD = padButtons.index(of: sender){
            pads.oneShot(pad: iD)
            
            //Send notification to TransportController. This is used when in record mode.
            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "pad\(iD)"), object: nil)
        }
    }
}

