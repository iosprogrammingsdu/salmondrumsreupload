//
//  PatternViewController.swift
//  AdvancedDrumMachineApp
//
//  Created by Morten Robinson on 01/12/2018.
//  Copyright © 2018 Morten Robinson. All rights reserved.
//

import UIKit

class PatternViewController: UIViewController {

    @IBOutlet var patternButtons: [UIButton]!
    
    var pattern : Pattern = Pattern()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    
    @IBAction func patternButtonTapped(_ sender: UIButton) {
        if let button = patternButtons.index(of: sender) {
            pattern.pattern[button/16][button%16] = !pattern.pattern[button/16][button%16]
        }
        updateView()
    }
    
    func updateView() {
        //post a patternChange notification so the sequencer can be updated
        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "patternChanged"), object: nil)
        
        
        for index in patternButtons.indices {
            let button = patternButtons[index]
            let step = pattern.pattern[index/16][index%16]
            if step {
                button.setTitle("🔴", for: UIControl.State.normal)
            }
            else {
                button.setTitle("⚫️", for: UIControl.State.normal)
            }
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
