//
//  patternSequencer.swift
//  machine898
//
//  Created by Morten Robinson on 27/11/2018.
//  Copyright © 2018 Barney Garda. All rights reserved.
//

import AudioKit

struct Metronome {
    
    var sequencer = AKSequencer()
    var metronome = AKMIDISampler()
    var currentTempo = 128.0 {
        didSet {
            sequencer.setTempo(currentTempo)
        }
    }
    
    let sequenceLength = AKDuration(beats: 4.0)
    let step = 1/16
    init() {

        initMetronome()
        
        AudioKit.output = metronome
        
        do {
            try AudioKit.start()
        } catch {
            AKLog("AudioKit did not start!")
        }
    }
    
    private func initMetronome() {
        do{
            let metronomeStrong = try AKAudioFile(readFileName: "Samples/MetronomeStrong_C2.wav")
            let metronomeWeak = try AKAudioFile(readFileName: "Samples/MetronomeWeak_C3.wav")
            try metronome.loadAudioFiles([metronomeStrong, metronomeWeak])
        } catch {
            AKLog("Files Didn't Load")
        }
        metronome.name = "metronome"
        
        //Enable midi and set volume
        metronome.enableMIDI()
        metronome.volume = 0

        //Create sequencer
        _ = sequencer.newTrack()
        sequencer.setLength(sequenceLength)
        sequencer.tracks[0].setMIDIOutput(metronome.midiIn)
        
        //Add metronome clicks to midi track
        sequencer.tracks[0].add(noteNumber: MIDINoteNumber(36), velocity: MIDIVelocity(100), position: AKDuration(beats: 0), duration: AKDuration(beats: 1))
        sequencer.tracks[0].add(noteNumber: MIDINoteNumber(48), velocity: MIDIVelocity(100), position: AKDuration(beats: 1), duration: AKDuration(beats: 1))
        sequencer.tracks[0].add(noteNumber: MIDINoteNumber(48), velocity: MIDIVelocity(100), position: AKDuration(beats: 2), duration: AKDuration(beats: 1))
        sequencer.tracks[0].add(noteNumber: MIDINoteNumber(48), velocity: MIDIVelocity(100), position: AKDuration(beats: 3), duration: AKDuration(beats: 1))
        
        //Loop 4 beats
        sequencer.debug()
        sequencer.enableLooping(AKDuration(beats: 4))
    }
    
    func play() {
        sequencer.play()
    }
    
    func stop() {
        sequencer.stop()
        sequencer.rewind()
    }
    
    func record() {
        sequencer.play()
    }
    
    func metronomeOnOff(state: Bool) {
        if state {
            metronome.volume = 64
        }
        else {
            metronome.volume = 0
        }
    }
}
