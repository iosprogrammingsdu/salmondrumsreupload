//
//  Pattern.swift
//  AdvancedDrumMachineApp
//
//  Created by Morten Robinson on 01/12/2018.
//  Copyright © 2018 Morten Robinson. All rights reserved.
//

import Foundation

struct Pattern {
    //var pattern : [[Bool?]]
    
    var pattern = Array(repeating: Array(repeating: false, count: 16), count: 10)
    
    mutating func clearPattern() {
        pattern = Array(repeating: Array(repeating: false, count: 16), count: 10)
    }
}
